#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.27-tsi_test-debug', 'build/scratch/subdir/ns3.27-subdir-debug', 'build/scratch/ns3.27-scratch-simulator-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

