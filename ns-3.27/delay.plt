set terminal png
set output "TSI_test_delay.png"
set title "TSI Test Delay"
set xlabel "Packet"
set ylabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1 
set grid ytics  
set grid xtics 
plot "delay.dat" title "TSI Test Delay" with linespoints  ls 1 